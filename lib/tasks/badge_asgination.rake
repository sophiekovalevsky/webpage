namespace :badge_asignation do
  desc "Asign badges to all attendies to event"
  def log(msg)
    puts msg
    Rails.logger.info(msg)
  end
  task :asign, [:event,:badge] => :environment do |t,args|
    log("Start assigning Badges")
    event = Event.find(args[:event])
    badge = Badge.find(args[:badge])
    event.attendees.each do |attendee|
      log("Add badge %{badge.name} to user %{attendee.user.name}")
      begin
        UserBadge.create!(user_id: attendee.user_id,badge_id: badge.id)
        BadgeMailer.new_badge(attendee.user,badge).deliver_now
      rescue
        log("error asgining badge to %{attendee.user.name} ")
      end
    end
  end

end
