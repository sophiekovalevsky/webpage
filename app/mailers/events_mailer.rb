class EventsMailer < ApplicationMailer
  default from: 'do-not-reply@floss-pa.net'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.events_mailer.invites.subject
  #
  def invites(user,event)
    @user=user
    @event=event
    mail(to: @user.email, subject: t(:attendees_invitation,
                                 user_name: @event.user.name.capitalize,
                                 event_name: @event.title) )
  end
end
