class UserController < ApplicationController
  skip_before_action :authenticate_user!, only: [:show,:assertions]
  before_action :set_user, only: [:show,:assertions]

  def show
    if @user
      @badges = @user.badges
    end
  end

  def events
      @events = current_user.events.paginate(:page => params[:page],:per_page =>8 ).order('start_at DESC')
      render file: 'events/index'
  end

  def assertions
    user_badges = UserBadge.where("user_id=?",@user.id)
    badges=[]
    user_badges.each do |user_badge|
      first=UserBadge.where("badge_id=?",user_badge.badge_id).first
      last=UserBadge.where("badge_id=?",user_badge.badge_id).last
      times=UserBadge.where("badge_id=?",user_badge.badge_id).count
      badges << {:description=>user_badge.badge.description,
                 :tags=>user_badge.badge.tags,
                 :issue=>user_badge.created_at.to_time.to_i,
                 :image=>URI.join(request.url, user_badge.badge.image.url),
                 :first_awarded=>first.created_at.to_time.to_i,
                 :first_awarded_person=>first.user.name,
                 :last_awarded_person=>last.user.name,
                 :last_awarded=>last.created_at.to_time.to_i,
                 :id=>user_badge.badge.uuid,
                 :time_awarded=>times,
                 :name=>user_badge.badge.name}
    end
    percentile=UserBadge.where("user_id=?",@user.id).count.to_f/Badge.count.to_f
    data= UserBadge.find_by_sql("select user_id,count(*)  from user_badges group by user_id order by 2 desc")
    count=0
    rank=0
    data.each do |d|
     count+=1
     rank=count if d["user_id"]==@user.id
    end
    assertions={:user_count=>User.count, :rank=>rank,:avatar=>URI.join(request.url, @user.avatar.url),:percentile=>percentile,:assertions=>badges}
    render json: assertions

  end

  private
  def set_user
    @user=User.find(params[:id])
  end
end
