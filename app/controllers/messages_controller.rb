class MessagesController < ApplicationController
    before_action :set_conversation
    def index
      #@message = @conversation.messages.new
      @messages = @conversation.messages

      @messages.where("user_id != ? AND readed = ?", current_user.id, false).update_all(readed: true)
      #logger.info @messages.inspect

      respond_to do |format|
          format.js
          format.html
          format.json
      end

    end

    def create
      @message = @conversation.messages.new(message_params)
      @message.user = current_user
      respond_to do |format|
        if @message.save
          format.js
          format.html {redirect_to conversations_url}
          format.json
        else
          format.js
        end
      end
    end

    private
      def message_params
        params.require(:message).permit(:body, :user_id)
      end

      def set_conversation
       @conversation = Conversation.find(params[:conversation_id])
      rescue ActiveRecord::RecordNotFound
       redirect_to conversations_url
      end
end
