class ScheduleController < ApplicationController
    before_action :set_event
    load_and_authorize_resource
  def index
      @schedules = @event.schedules.empty? ? @event.schedules.new : @event.schedules
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_event
    numeric =  Float(params[:event_id]) != nil rescue false
    if numeric
      @event = Event.find(params[:event_id])
    else
        @event = Event.find_by_subdomain(params[:event_id])
    end
  end
end
