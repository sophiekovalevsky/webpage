class Conversation < ApplicationRecord
  belongs_to :user
  belongs_to :recipient, :foreign_key => :recipient_id, class_name: :User
  validates_presence_of :user
  has_many :messages, dependent: :destroy
  validates_uniqueness_of :user_id, :scope => :recipient_id
  accepts_nested_attributes_for :messages
  scope :between, -> (user_id,recipient_id) do
    where("(conversations.user_id = ? AND conversations.recipient_id =?) OR (conversations.user_id = ? AND conversations.recipient_id =?)", user_id,recipient_id, recipient_id, user_id)
 end
end
