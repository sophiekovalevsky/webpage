class Attendee < ApplicationRecord
  belongs_to :ticket
  belongs_to :user
  has_one :event, through: :ticket
  validates :ticket, uniqueness: { scope: :user}
  after_create :send_ticket, :create_notification

  private

  def send_ticket
    unless self.user.email.include?('facebook')
      cal=self.event.create_calendar_entry
      TicketMailer.new_ticket(self.id,cal.to_ical).deliver_later
    end
  end

  def recipients
    [self.event]
  end
  def create_notification
    recipients.each do |recipient|
      Notification.create(recipient: recipient.user, user: user, notifiable: self, action: 'going')
    end
  end
end
