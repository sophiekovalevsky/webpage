class BoardMessage < ApplicationRecord
  belongs_to :board
  #belongs_to :user
  validates_presence_of :board
  #validates_presence_of :user
  before_save :add_user
  after_create :create_notification

  private

  def add_user
    logger.info self.user_id
    self.user_id=self.board.user_id if self.user_id.nil?
  end
  def recipients
    self.board.community.members
  end
  def create_notification
    recipients.each do |recipient|
      Notification.create(recipient: recipient.user, user: User.find(self.user_id), notifiable: self, action: 'message')
    end
  end
end
