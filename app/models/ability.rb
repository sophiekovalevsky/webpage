class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
    can :read, :all
    #can :read, :home, :all
    can :calendar_download, Event
    return unless user.present?
    can :manage, News, user_id: user.id
    can :manage, Community, user_id: user.id
    can :manage, Event, user_id: user.id
    can :manage, Badge, owner_id: user.id
    can :manage, Invite, user_id: user.id
    can :manage, Conversation, user_id: user.id
    can [:read,:create], Ticket, user_id: user.id
    can [:read,:create], Attendee, user_id: user.id
    can [:read,:create], UserBadge, user_id: user.id
    can [:read,:create], Member, user_id: user.id
    #can [:read,:create], Board, user_id: user.id
    #can [:read,:create], Member, user_id: user.id
    can [:read,:create], Notification, user_id: user.id
    can [:read], Notification, recipient_id: user.id
    can [:update], Member, :read_at => DateTime.now.to_s(:db), recipient_id: user.id
    can [:read,:create], Message, user_id: user.id
    can [:read,:create], Board, user_id: user.id 
    can [:read,:create], BoardMessage, user_id: user.id
    can [:read,:create], BoardMessage, board: {community: {members: {user: {id: user.id }}}}
    can :manage, BoardMessage, board: {community: {member: {user: {id: user.id }}}}
    can :update, Invite, :accepted => true, to_user_id: user.id
    can :manage, :all if user.has_role? :admin

  end
end
