class Board < ApplicationRecord
  belongs_to :user
  belongs_to :community
  has_many :board_messages
  validates_presence_of :user
  validates_presence_of :community
  validates :subject, uniqueness: true

  accepts_nested_attributes_for :board_messages #, reject_if: :board_message_check
  after_create :create_notification

  def message_count
    self.board_messages.where("deleted=false").count
  end
  private

  def board_message_check(attributes)
    attributes['content'].blank?
  end

  def recipients
    self.community.members.where("admin=true")
  end

  def create_notification
    recipients.each do |recipient|
      Notification.create(recipient: recipient.user, user: user, notifiable: self, action: 'subject')
    end
  end
end
