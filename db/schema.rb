# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190820223949) do

  create_table "activities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", limit: 50
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_activities_on_user_id"
  end

  create_table "addresses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "country_id"
    t.bigint "state_id"
    t.bigint "district_id"
    t.bigint "county_id"
    t.string "address"
    t.float "lat", limit: 24
    t.float "long", limit: 24
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "client_id"
    t.string "street"
    t.string "neighborhood"
    t.string "building"
    t.string "house"
    t.index ["client_id"], name: "index_addresses_on_client_id"
    t.index ["country_id"], name: "index_addresses_on_country_id"
    t.index ["county_id"], name: "index_addresses_on_county_id"
    t.index ["district_id"], name: "index_addresses_on_district_id"
    t.index ["state_id"], name: "index_addresses_on_state_id"
  end

  create_table "agencies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", limit: 50
    t.string "address", limit: 150
    t.string "state", limit: 50
    t.string "district", limit: 50
    t.string "county", limit: 50
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ip_patern"
    t.bigint "address_id"
    t.index ["address_id"], name: "index_agencies_on_address_id"
    t.index ["user_id"], name: "index_agencies_on_user_id"
  end

  create_table "agents", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "first_name", limit: 50
    t.string "last_name", limit: 50
    t.string "cedula", limit: 16
    t.bigint "position_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pos_username"
    t.index ["position_id"], name: "index_agents_on_position_id"
    t.index ["user_id"], name: "index_agents_on_user_id"
  end

  create_table "almost_cashes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assignments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_assignments_on_role_id"
    t.index ["user_id"], name: "index_assignments_on_user_id"
  end

  create_table "attendees", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "ticket_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "validated", default: false
    t.datetime "validated_at"
    t.boolean "won", default: false
    t.index ["ticket_id"], name: "index_attendees_on_ticket_id"
    t.index ["user_id"], name: "index_attendees_on_user_id"
  end

  create_table "badge_rules", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.integer "times"
    t.boolean "inmediatly"
    t.string "badgeble_type", limit: 50
    t.string "relation_class", limit: 50
    t.string "deliver", limit: 50
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "badges", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "owner_id"
    t.string "uuid", null: false
    t.string "name"
    t.string "description"
    t.string "criteria"
    t.datetime "expire"
    t.string "tags"
    t.datetime "claim_until"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.bigint "image_file_size"
    t.datetime "image_updated_at"
    t.bigint "badgeble_id"
    t.string "badgeble_type", limit: 50
    t.bigint "badge_rule_id"
    t.index ["badge_rule_id"], name: "index_badges_on_badge_rule_id"
    t.index ["badgeble_id", "badgeble_type"], name: "index_badges_on_badgeble_id_and_badgeble_type"
    t.index ["uuid"], name: "index_badges_on_uuid", unique: true
  end

  create_table "board_messages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "board_id"
    t.bigint "user_id"
    t.text "content"
    t.integer "views"
    t.boolean "read"
    t.boolean "deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board_id"], name: "index_board_messages_on_board_id"
    t.index ["user_id"], name: "index_board_messages_on_user_id"
  end

  create_table "boards", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "community_id"
    t.bigint "user_id"
    t.string "subject"
    t.integer "views"
    t.boolean "pin"
    t.boolean "deleted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_boards_on_community_id"
    t.index ["user_id"], name: "index_boards_on_user_id"
  end

  create_table "bootsy_image_galleries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "bootsy_resource_type"
    t.bigint "bootsy_resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bootsy_resource_type", "bootsy_resource_id"], name: "bootsy_res_image_gal"
  end

  create_table "bootsy_images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "image_file"
    t.bigint "image_gallery_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["image_gallery_id"], name: "bootsy_img_gallery"
  end

  create_table "carousels", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.string "header"
    t.string "sentence"
    t.string "link"
    t.string "image"
    t.bigint "page_id"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.index ["page_id"], name: "index_carousels_on_page_id"
    t.index ["user_id"], name: "index_carousels_on_user_id"
  end

  create_table "cash_deposits", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "agent_id"
    t.bigint "po_id"
    t.string "journal_id"
    t.string "procesor", default: "Cash"
    t.string "transaction_id"
    t.boolean "state"
    t.string "client"
    t.string "name"
    t.string "last_name"
    t.float "amount", limit: 24
    t.string "currency", default: "USD"
    t.string "type"
    t.date "time_date"
    t.string "added_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["agent_id"], name: "index_cash_deposits_on_agent_id"
    t.index ["po_id"], name: "index_cash_deposits_on_po_id"
    t.index ["user_id"], name: "index_cash_deposits_on_user_id"
  end

  create_table "cash_payments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "po_id"
    t.bigint "agent_id"
    t.string "journal_id"
    t.string "procesesor"
    t.string "transaction_id"
    t.boolean "state"
    t.string "location"
    t.string "client"
    t.string "name"
    t.string "last_name"
    t.float "amount", limit: 24
    t.float "tax", limit: 24
    t.float "payed", limit: 24
    t.string "hour"
    t.string "added_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["agent_id"], name: "index_cash_payments_on_agent_id"
    t.index ["po_id"], name: "index_cash_payments_on_po_id"
    t.index ["user_id"], name: "index_cash_payments_on_user_id"
  end

  create_table "cash_registers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "pos_id"
    t.bigint "till_id"
    t.bigint "agent_id"
    t.bigint "turn_id"
    t.bigint "user_id"
    t.float "fund", limit: 24
    t.float "account_fund", limit: 24
    t.float "income", limit: 24
    t.float "account_deposits", limit: 24
    t.float "income_transfer", limit: 24
    t.float "client_income", limit: 24
    t.float "refill_by_check", limit: 24
    t.integer "check"
    t.float "payouts", limit: 24
    t.float "account_payments", limit: 24
    t.float "cancels", limit: 24
    t.integer "ones"
    t.integer "fives"
    t.integer "tents"
    t.integer "twenties"
    t.integer "fiftys"
    t.integer "hundreds"
    t.integer "coins_ones"
    t.integer "coins_tens"
    t.integer "coins_fives"
    t.integer "coins_twenty_fives"
    t.integer "coins_fifties"
    t.float "extra", limit: 24
    t.string "observations"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "reviewed_by"
    t.datetime "reviewed_at"
    t.bigint "approved_by"
    t.datetime "approved_at"
    t.index ["agent_id"], name: "index_cash_registers_on_agent_id"
    t.index ["pos_id"], name: "index_cash_registers_on_pos_id"
    t.index ["turn_id"], name: "index_cash_registers_on_turn_id"
    t.index ["user_id"], name: "index_cash_registers_on_user_id"
  end

  create_table "checks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "cash_register_id"
    t.integer "number"
    t.string "issue_by"
    t.string "bank"
    t.float "amount", limit: 24
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cash_register_id"], name: "index_checks_on_cash_register_id"
  end

  create_table "client_m", primary_key: "ID", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "active"
    t.string "cuenta"
    t.string "provincia"
    t.string "distrito"
    t.string "mix_add"
    t.string "email"
    t.string "nombre"
    t.string "telefono"
    t.string "home_phone"
    t.string "celular"
    t.string "account"
    t.string "direccion"
    t.string "barrio"
    t.string "cedula"
    t.string "ocupacion"
    t.string "nacionalidad"
    t.string "empleado"
    t.string "fecha_apertura"
    t.string "razon"
  end

  create_table "client_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", limit: 50
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code", limit: 3
    t.index ["user_id"], name: "index_client_types_on_user_id"
  end

  create_table "clients", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "account", limit: 16
    t.string "cedula", limit: 16
    t.string "first_name", limit: 50
    t.string "last_name", limit: 50
    t.bigint "activity_id"
    t.string "phone", limit: 15
    t.string "cellphone", limit: 15
    t.bigint "agent_id"
    t.bigint "client_type_id"
    t.string "email", limit: 120
    t.date "birthday"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "picture_file_name"
    t.string "picture_content_type"
    t.integer "picture_file_size"
    t.datetime "picture_updated_at"
    t.bigint "id_type_id"
    t.boolean "peps"
    t.string "position"
    t.string "entity"
    t.string "reason"
    t.boolean "foraigners"
    t.boolean "beneficiary"
    t.string "credit_card_name"
    t.integer "last_four_digits"
    t.string "credit_card_type"
    t.string "other_card"
    t.string "expiration_date"
    t.bigint "country_id"
    t.bigint "agency_id"
    t.index ["activity_id"], name: "index_clients_on_activity_id"
    t.index ["agency_id"], name: "index_clients_on_agency_id"
    t.index ["agent_id"], name: "index_clients_on_agent_id"
    t.index ["client_type_id"], name: "index_clients_on_client_type_id"
    t.index ["country_id"], name: "index_clients_on_country_id"
    t.index ["id_type_id"], name: "index_clients_on_id_type_id"
    t.index ["user_id"], name: "index_clients_on_user_id"
  end

  create_table "communities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.string "name"
    t.text "information"
    t.string "logo"
    t.string "site"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo_file_name"
    t.string "logo_content_type"
    t.integer "logo_file_size"
    t.datetime "logo_updated_at"
    t.string "subdomain", limit: 50
    t.index ["subdomain"], name: "index_communities_on_subdomain", unique: true
    t.index ["user_id"], name: "index_communities_on_owner_id"
  end

  create_table "contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "community_id"
    t.bigint "user_id"
    t.boolean "reply", default: false
    t.string "members"
    t.string "subject"
    t.text "message"
    t.boolean "publish_to_board", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_contacts_on_community_id"
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "conversations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.integer "recipient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_conversations_on_user_id"
  end

  create_table "counties", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "district_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["district_id"], name: "index_counties_on_district_id"
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "credit_cards", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "cash_register_id"
    t.string "ticket"
    t.string "sold_by"
    t.float "amount", limit: 24
    t.string "observation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cash_register_id"], name: "index_credit_cards_on_cash_register_id"
  end

  create_table "credit_payments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "rec_collect"
    t.string "name"
    t.float "amount", limit: 24
    t.string "observation"
    t.string "issue_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_credit_payments_on_user_id"
  end

  create_table "credits", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "rec_credit"
    t.string "name"
    t.float "amount", limit: 24
    t.string "observation"
    t.string "issue_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_credits_on_user_id"
  end

  create_table "deposits", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "recipt"
    t.bigint "client_id"
    t.bigint "agency_id"
    t.bigint "transaction_type_id"
    t.string "check_number"
    t.string "bank"
    t.datetime "date"
    t.bigint "agent_id"
    t.decimal "amount", precision: 12, scale: 2
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cash_register_id"
    t.index ["agency_id"], name: "index_deposits_on_agency_id"
    t.index ["agent_id"], name: "index_deposits_on_agent_id"
    t.index ["cash_register_id"], name: "index_deposits_on_cash_register_id"
    t.index ["client_id"], name: "index_deposits_on_client_id"
    t.index ["transaction_type_id"], name: "index_deposits_on_transaction_type_id"
    t.index ["user_id"], name: "index_deposits_on_user_id"
  end

  create_table "distribution_channels", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "districts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "state_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_districts_on_state_id"
  end

  create_table "events", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.string "title"
    t.string "image"
    t.string "location"
    t.string "frecuency"
    t.datetime "start_at"
    t.datetime "end_at"
    t.text "description"
    t.string "keyworkds"
    t.string "ticket_url"
    t.bigint "community_id"
    t.string "organizer"
    t.boolean "publish"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.float "latitude", limit: 24
    t.float "longitude", limit: 24
    t.string "subdomain", limit: 50
    t.index ["community_id"], name: "index_events_on_community_id"
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "fund_details", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "pos_id"
    t.integer "ones"
    t.integer "fives"
    t.integer "tens"
    t.integer "twenties"
    t.integer "fifty"
    t.integer "hundres"
    t.integer "pennt"
    t.integer "nickle"
    t.integer "dime"
    t.integer "quarter"
    t.integer "half"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "pos_registry_id"
    t.index ["pos_id"], name: "index_fund_details_on_pos_id"
    t.index ["pos_registry_id"], name: "index_fund_details_on_pos_registry_id"
  end

  create_table "id_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "code", limit: 3
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "identities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "provider"
    t.string "uid"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "invitation_sends", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "invite_id"
    t.integer "to_user_id"
    t.string "email"
    t.datetime "accepted"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invite_id"], name: "index_invitation_sends_on_invite_id"
  end

  create_table "invites", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "community_id"
    t.bigint "user_id"
    t.integer "to_user_id"
    t.string "email"
    t.text "body"
    t.datetime "accepted"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_invites_on_community_id"
    t.index ["user_id"], name: "index_invites_on_user_id"
  end

  create_table "marketing_hows", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marketing_influencers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marketing_tvs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marketings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "marketing_how_id"
    t.bigint "marketing_influencer_id"
    t.bigint "marketing_tv_id"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_marketings_on_client_id"
    t.index ["marketing_how_id"], name: "index_marketings_on_marketing_how_id"
    t.index ["marketing_influencer_id"], name: "index_marketings_on_marketing_influencer_id"
    t.index ["marketing_tv_id"], name: "index_marketings_on_marketing_tv_id"
  end

  create_table "members", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "community_id"
    t.bigint "user_id"
    t.boolean "admin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "removed", default: false
    t.boolean "ban"
    t.integer "ban_by"
    t.integer "removed_by"
    t.index ["community_id", "user_id"], name: "index_members_on_community_id_and_user_id", unique: true
    t.index ["community_id"], name: "index_members_on_community_id"
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "messages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "conversation_id"
    t.string "subject"
    t.text "body"
    t.boolean "readed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "news", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.string "title"
    t.string "keywords"
    t.string "language", limit: 2
    t.boolean "publish"
    t.datetime "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_news_on_user_id"
  end

  create_table "news_contents", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "news_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["news_id"], name: "index_news_contents_on_news_id"
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.integer "recipient_id"
    t.datetime "read_at"
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.string "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "occupations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oldusers", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "operation_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.string "title"
    t.string "language"
    t.string "keywords"
    t.text "content"
    t.boolean "carousel"
    t.boolean "is_publish"
    t.boolean "in_menu"
    t.string "menu_label"
    t.boolean "is_home"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_pages_on_user_id"
  end

  create_table "payments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "client_id"
    t.bigint "agency_id"
    t.bigint "agent_id"
    t.bigint "transaction_type_id"
    t.decimal "amount", precision: 12, scale: 2
    t.decimal "tax", precision: 12, scale: 2
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "cash_register_id"
    t.index ["agency_id"], name: "index_payments_on_agency_id"
    t.index ["agent_id"], name: "index_payments_on_agent_id"
    t.index ["cash_register_id"], name: "index_payments_on_cash_register_id"
    t.index ["client_id"], name: "index_payments_on_client_id"
    t.index ["transaction_type_id"], name: "index_payments_on_transaction_type_id"
    t.index ["user_id"], name: "index_payments_on_user_id"
  end

  create_table "point_of_sales", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "agent_id"
    t.bigint "agency_id"
    t.date "date"
    t.bigint "till_id"
    t.string "reviewed_by"
    t.string "approved_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.bigint "type_id"
    t.index ["agency_id"], name: "index_pos_on_agency_id"
    t.index ["agent_id"], name: "index_pos_on_agent_id"
    t.index ["user_id"], name: "index_pos_on_user_id"
  end

  create_table "pos_registries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "pos_id"
    t.float "fund", limit: 24
    t.float "income", limit: 24
    t.float "income_transfer", limit: 24
    t.float "client_income", limit: 24
    t.float "refill_by_check", limit: 24
    t.integer "check_id"
    t.float "payouts", limit: 24
    t.float "cancels", limit: 24
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "turn_id"
    t.index ["pos_id"], name: "index_pos_registries_on_pos_id"
    t.index ["turn_id"], name: "index_pos_registries_on_turn_id"
  end

  create_table "positions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", limit: 50
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_positions_on_user_id"
  end

  create_table "product_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rounds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedules", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "event_id"
    t.bigint "user_id"
    t.string "speaker_name", limit: 150
    t.string "email"
    t.text "speaker_resume"
    t.string "talk_name"
    t.text "talk_resume"
    t.string "room", limit: 50
    t.boolean "workshop", default: false
    t.datetime "date_hour"
    t.time "hour_start"
    t.time "hour_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_schedules_on_event_id"
    t.index ["user_id"], name: "index_schedules_on_user_id"
  end

  create_table "states", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "country_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_states_on_country_id"
  end

  create_table "ticket_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tickets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "event_id"
    t.integer "amount"
    t.bigint "ticket_type_id"
    t.string "token", limit: 50
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_tickets_on_event_id"
    t.index ["ticket_type_id"], name: "index_tickets_on_ticket_type_id"
    t.index ["token"], name: "index_tickets_on_token", unique: true
  end

  create_table "tills", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "agency_id"
    t.string "csv_file"
    t.datetime "time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "csv_file_file_name"
    t.string "csv_file_content_type"
    t.integer "csv_file_file_size"
    t.datetime "csv_file_updated_at"
    t.index ["agency_id"], name: "index_tills_on_agency_id"
    t.index ["user_id"], name: "index_tills_on_user_id"
  end

  create_table "tmp_event", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.datetime "event_start", null: false
    t.datetime "event_end", null: false
    t.string "title", default: "", null: false, collation: "utf8_general_ci"
    t.text "body", limit: 4294967295, null: false, collation: "utf8_general_ci"
  end

  create_table "tmp_news", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "mail", limit: 64, default: "", collation: "utf8_general_ci"
    t.string "language", limit: 12, default: "", null: false, collation: "utf8_general_ci"
    t.string "title", default: "", null: false, collation: "utf8_general_ci"
    t.text "body", limit: 4294967295, null: false, collation: "utf8_general_ci"
    t.integer "created", default: 0, null: false
    t.integer "changed", default: 0, null: false
  end

  create_table "transaction_reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "ticket"
    t.string "name"
    t.string "cid"
    t.string "address"
    t.string "occupetion"
    t.float "bet", limit: 24
    t.float "payment", limit: 24
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_transaction_reports_on_user_id"
  end

  create_table "transaction_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name", limit: 50
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_transaction_types_on_user_id"
  end

  create_table "turns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "uaf_transaction_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_badges", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "badge_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["badge_id"], name: "index_user_badges_on_badge_id"
    t.index ["user_id", "badge_id"], name: "index_user_badges_on_user_id_and_badge_id", unique: true
    t.index ["user_id"], name: "index_user_badges_on_user_id"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "versions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4" do |t|
    t.string "item_type", limit: 191, null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object", limit: 4294967295
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "activities", "users"
  add_foreign_key "addresses", "clients"
  add_foreign_key "addresses", "counties"
  add_foreign_key "addresses", "countries"
  add_foreign_key "addresses", "districts"
  add_foreign_key "addresses", "states"
  add_foreign_key "agencies", "addresses"
  add_foreign_key "agencies", "users"
  add_foreign_key "agents", "positions"
  add_foreign_key "agents", "users"
  add_foreign_key "assignments", "roles"
  add_foreign_key "assignments", "users"
  add_foreign_key "attendees", "tickets"
  add_foreign_key "attendees", "users"
  add_foreign_key "board_messages", "boards"
  add_foreign_key "board_messages", "users"
  add_foreign_key "boards", "users"
  add_foreign_key "carousels", "pages"
  add_foreign_key "carousels", "users"
  add_foreign_key "checks", "cash_registers"
  add_foreign_key "client_types", "users"
  add_foreign_key "clients", "activities"
  add_foreign_key "clients", "agencies"
  add_foreign_key "clients", "agents"
  add_foreign_key "clients", "client_types"
  add_foreign_key "clients", "countries"
  add_foreign_key "clients", "id_types"
  add_foreign_key "clients", "users"
  add_foreign_key "communities", "users"
  add_foreign_key "contacts", "communities"
  add_foreign_key "contacts", "users"
  add_foreign_key "conversations", "users"
  add_foreign_key "counties", "districts"
  add_foreign_key "credit_cards", "cash_registers"
  add_foreign_key "credit_payments", "users"
  add_foreign_key "credits", "users"
  add_foreign_key "deposits", "agencies"
  add_foreign_key "deposits", "agents"
  add_foreign_key "deposits", "cash_registers"
  add_foreign_key "deposits", "clients"
  add_foreign_key "deposits", "transaction_types"
  add_foreign_key "deposits", "users"
  add_foreign_key "districts", "states"
  add_foreign_key "events", "communities"
  add_foreign_key "events", "users"
  add_foreign_key "fund_details", "pos", column: "pos_id"
  add_foreign_key "identities", "users"
  add_foreign_key "invitation_sends", "invites"
  add_foreign_key "invites", "communities"
  add_foreign_key "invites", "users"
  add_foreign_key "marketings", "clients"
  add_foreign_key "marketings", "marketing_hows"
  add_foreign_key "marketings", "marketing_influencers"
  add_foreign_key "marketings", "marketing_tvs"
  add_foreign_key "members", "communities"
  add_foreign_key "members", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "news", "users"
  add_foreign_key "news_contents", "news"
  add_foreign_key "notifications", "users"
  add_foreign_key "pages", "users"
  add_foreign_key "payments", "agencies"
  add_foreign_key "payments", "agents"
  add_foreign_key "payments", "cash_registers"
  add_foreign_key "payments", "clients"
  add_foreign_key "payments", "transaction_types"
  add_foreign_key "payments", "users"
  add_foreign_key "pos", "agencies"
  add_foreign_key "pos", "agents"
  add_foreign_key "pos", "users"
  add_foreign_key "pos_registries", "pos", column: "pos_id"
  add_foreign_key "pos_registries", "turns"
  add_foreign_key "positions", "users"
  add_foreign_key "schedules", "events"
  add_foreign_key "schedules", "users"
  add_foreign_key "states", "countries"
  add_foreign_key "tickets", "events"
  add_foreign_key "tickets", "ticket_types"
  add_foreign_key "tills", "agencies"
  add_foreign_key "tills", "users"
  add_foreign_key "transaction_reports", "users"
  add_foreign_key "transaction_types", "users"
  add_foreign_key "user_badges", "badges"
  add_foreign_key "user_badges", "users"
end
