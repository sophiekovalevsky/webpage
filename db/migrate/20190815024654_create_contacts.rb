class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.references :community, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :reply, default: false
      t.string :members
      t.string :subject
      t.text :message
      t.boolean :publish_to_board, default: false

      t.timestamps
    end
  end
end
