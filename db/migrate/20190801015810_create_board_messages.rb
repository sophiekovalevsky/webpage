class CreateBoardMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :board_messages do |t|
      t.references :board, foreign_key: true
      t.references :user, foreign_key: true
      t.text :content
      t.integer :views
      t.boolean :read
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
