class CreateSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
      t.references :event, foreign_key: true
      t.references :user, foreign_key: true
      t.string :speaker_name, limit: 150
      t.string :email
      t.text :speaker_resume, limit: 500
      t.string :talk_name
      t.text :talk_resume, limit: 500
      t.string :room, limit: 50
      t.boolean :workshop, default: false

      t.datetime :date_hour
      t.time :hour_start
      t.time :hour_end

      t.timestamps
    end
  end
end
