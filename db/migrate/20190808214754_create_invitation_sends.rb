class CreateInvitationSends < ActiveRecord::Migration[5.1]
  def change
    create_table :invitation_sends do |t|
      t.references :invite, foreign_key: true
      t.integer :to_user_id
      t.string :email
      t.datetime :accepted
      t.string :uuid

      t.timestamps
    end
  end
end
