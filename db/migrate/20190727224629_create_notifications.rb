class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.references :user, foreign_key: true
      t.integer :recipient_id
      t.datetime :read_at
      t.integer :notifiable_id
      t.string :notifiable_type
      t.string :action

      t.timestamps
    end
  end
end
