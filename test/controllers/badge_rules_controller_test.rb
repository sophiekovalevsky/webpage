require 'test_helper'

class BadgeRulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @badge_rule = badge_rules(:one)
  end

  test "should get index" do
    get badge_rules_url
    assert_response :success
  end

  test "should get new" do
    get new_badge_rule_url
    assert_response :success
  end

  test "should create badge_rule" do
    assert_difference('BadgeRule.count') do
      post badge_rules_url, params: { badge_rule: { badgeble_type: @badge_rule.badgeble_type, deliver: @badge_rule.deliver, inmediatly: @badge_rule.inmediatly, name: @badge_rule.name, times: @badge_rule.times } }
    end

    assert_redirected_to badge_rule_url(BadgeRule.last)
  end

  test "should show badge_rule" do
    get badge_rule_url(@badge_rule)
    assert_response :success
  end

  test "should get edit" do
    get edit_badge_rule_url(@badge_rule)
    assert_response :success
  end

  test "should update badge_rule" do
    patch badge_rule_url(@badge_rule), params: { badge_rule: { badgeble_type: @badge_rule.badgeble_type, deliver: @badge_rule.deliver, inmediatly: @badge_rule.inmediatly, name: @badge_rule.name, times: @badge_rule.times } }
    assert_redirected_to badge_rule_url(@badge_rule)
  end

  test "should destroy badge_rule" do
    assert_difference('BadgeRule.count', -1) do
      delete badge_rule_url(@badge_rule)
    end

    assert_redirected_to badge_rules_url
  end
end
