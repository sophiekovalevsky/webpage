Rails.application.routes.draw do


  resources :conversations, only: [:index,:create,:new] do
    resources :messages, only: [:index,:create]
  end

  get 'communities/:community/events' => 'events#index', as: 'community_events'
  get 'communities/:community_id/contacts/preview' => "contacts#preview", as: 'community_contact_preview'
  resources :badge_rules
  get 'badges/:uuid/claim' => "badges#claim", as: 'badge_claim'
  get 'badges/:uuid/qrcode' => "badges#qrcode", as: 'qrcode_badge'
  get 'badges/:id/assertion' => "badges#assertion", as: 'badge_assertion'
  resources :badges
  resources :attendees
  resources :tickets
  resources :ticket_types

  resources :notifications, only: [:index]
  get "events/:id/downloads" => "events#calendar_download", as: 'event_calendar_download'
  get "event/:year/:month/:day" => "events#index", constraints: { year: /\d{4}/, month: /\d{2}/, day: /\d{2}/ }
  get "event/:year/:month/:day/*anything" => "events#index", constraints: { year: /\d{4}/, month: /\d{2}/, day: /\d{2}/ }
  get "event/badges/:event_id" => "badges#index", as: "event_badges"
  get 'privacy' => 'home#privacy',  :defaults => { :id => '2' }, as: 'privacy'
  get 'news/:id/edit' => 'news#edit'
  #get 'communities/:id/edit' => 'communities#edit'
  get 'news/:id/*anything' => 'news#show', as: 'news_custom'
  #get 'communities/:id/*anything' => 'communities#show', as: 'community_custom'
  get 'user/:id/*anything' => 'user#show', as: 'user_custom'
  get 'users/:id/badges' => 'user#assertions', as: 'user_badges'
  get 'users/events' => 'user#events', as: 'my_events'

  post 'events/:id/unique' => 'events#unique', as: 'unique_event'
  post 'events/unique' => 'events#unique', as: 'new_unique_event'
  post 'events/:event_id/attendees/draw' => 'attendees#draw', as: 'draw_attendees'

  resources :events do
    resources :attendees, only: [:index]
    resources :schedule, only: [:index]
  end
  resources :user, only: [:show]
  resources :communities do
    resources :members
    resources :invites
    resources :boards do
      resources :board_messages, only: [:new,:edit,:show,:update,:create,:destroy]
    end
    resources :contacts
  end
  resources :carousels
  resources :pages
   mount Bootsy::Engine => '/bootsy', as: 'bootsy'
  resources :news
  get 'home/index'

  devise_for :users,  :controllers => { omniauth_callbacks: 'omniauth_callbacks' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
end
